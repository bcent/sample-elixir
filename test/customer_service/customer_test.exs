defmodule CustomerService.CustomerTest do
  use ExUnit.Case
  # use Plug.Test

  alias CustomerService.Customer


  setup context do
    context
  end

  describe "create/1" do
    test "with good data" do
      customer = Customer.create(%{name: "test"})
      assert is_map(customer)
    end
  end

  describe "find/1" do
    test "with good data" do
      customer = Customer.create(%{name: "test"})
      second = Customer.find(customer.id)
      assert customer == second
    end

    test "with bad data" do
      customer = Customer.find(0)
      assert is_nil(customer)
    end
  end
end
