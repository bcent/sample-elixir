defmodule CustomerService.AuthTest do
  use ExUnit.Case
  # use Plug.Test

  alias CustomerService.Auth


  setup context do
    context
  end

  describe "fetch_token/1" do
    test "with good id" do
      result = Auth.fetch_token("example-readwrite")

      assert result.id == "example-readwrite"
      assert result.permissions == ["CanReadCustomer", "CanWriteCustomer"]
    end

    test "with bad id" do
      result = Auth.fetch_token("bad-token")

      assert is_nil(result)
    end
  end
end
