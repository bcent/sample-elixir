defmodule CustomerServiceWeb.CustomerControllerTest do
  use CustomerServiceWeb.ConnCase

  alias CustomerService.Customer

  setup %{conn: conn} do
    conn = conn
    |> put_req_header("accept", "application/json")

    %{
      read_conn: put_req_header(conn, "auth_token", "example-readonly"),
      write_conn: put_req_header(conn, "auth_token", "example-readwrite"),
      conn: conn
    }
  end

  describe "index" do
    test "lists all customers", %{read_conn: conn} do
      resp = get conn, "/api/v1/customers"
      assert json_response(resp, 200)["data"] == []
    end

    test "errors with bad auth token", %{conn: conn} do
      resp = get conn, "/api/v1/customers"
      assert response(resp, 401)
    end
  end

  describe "create customer" do
    test "renders customer when data is valid", %{write_conn: conn} do
      resp = post conn, "/api/v1/customers", name: "TEST"
      assert %{"id" => id} = json_response(resp, 201)["data"]

      resp = get conn, "/api/v1/customers/#{id}"
      assert json_response(resp, 200)["data"]["id"] == id
    end

    test "renders errors when data is invalid", %{write_conn: conn} do
      resp = post conn, "/api/v1/customers", %{}
      assert response(resp, 400)
    end

    test "renders errors when permission is lacking", %{read_conn: conn} do
      resp = post conn, "/api/v1/customers", name: "TEST"
      assert response(resp, 403)
    end

    test "renders errors when token is missing", %{conn: conn} do
      resp = post conn, "/api/v1/customers", name: "TEST"
      assert response(resp, 401)
    end

    test "high concurrency", %{write_conn: conn} do

      tasks = Enum.map(1..10, fn x ->
        Task.async(fn ->
          Enum.each(1..10, fn y ->
            IO.puts "Thread #{x} -  Customer #{y}"
            post conn, "/api/v1/customers", name: "TEST"
          end)
        end)
      end)

      Enum.each(tasks, fn x -> Task.await(x) end)
      conn = get conn, "/api/v1/customers"
      assert Enum.count(json_response(conn, 200)["data"]) == 100
    end
  end

  describe "update customer" do
    setup [:create_customer]

    test "renders customer when data is valid", %{write_conn: conn, customer: %Customer{id: id} = customer} do
      resp = put conn, "/api/v1/customers/#{id}", name: "update-test"
      assert json_response(resp, 200)["data"]["id"] == id

      resp = get conn, "/api/v1/customers/#{id}"
      assert json_response(resp, 200)["data"]["id"] == id
      assert json_response(resp, 200)["data"]["name"] == "update-test"
    end
  end

  describe "delete customer" do
    setup [:create_customer]

    test "deletes chosen customer", %{write_conn: conn, customer: customer} do
      resp = delete conn, "/api/v1/customers/#{customer.id}"
      assert response(resp, 204)

      resp = get conn, "/api/v1/customers/#{customer.id}"
      assert response(resp, 404)
    end
  end

  defp create_customer(_) do
    customer = Customer.create(%Customer{name: "TEST CUSTOMER"})
    {:ok, customer: customer}
  end
end
