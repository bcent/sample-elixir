defmodule CustomerServiceWeb.CustomerView do
  use CustomerServiceWeb, :view
  alias CustomerServiceWeb.CustomerView

  def render("index.json", %{customers: customers}) do
    %{data: render_many(customers, CustomerView, "customer.json")}
  end

  def render("show.json", %{customer: customer}) do
    %{data: render_one(customer, CustomerView, "customer.json")}
  end

  def render("customer.json", %{customer: customer}) do
    %{
      id: customer.id,
      name: customer.name,
      created_at: NaiveDateTime.to_iso8601(customer.created_at),
      last_logged_in_at: NaiveDateTime.to_iso8601(customer.last_logged_in_at)
    }
  end
end
