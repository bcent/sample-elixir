defmodule CustomerServiceWeb.CustomerController do
  @moduledoc """
    Customer Controller - accepts request passed along to it
    from the Router.

    Authentication is handled outside in two places - an Application
    wide plug - CustomerService.Auth.Plug, which will look up auth tokens,
    and assign them to the request. It will return 401 in the event that
    the token doesn't exist, or isn't provided.

    The second is imported to the controller, the CustomerService.Auth.authenticate/2
    function will check the token's permissions against the required permission.
    If the permission isn't found it will return forbiden
  """
  use CustomerServiceWeb, :controller
  alias CustomerService.Customer

  # This is where we configure the permissions
  import CustomerService.Auth, only: [authenticate: 2]
  plug :authenticate, "CanReadCustomer" when action in [:index, :show]
  plug :authenticate, "CanWriteCustomer" when action in [:create, :update, :delete]

  @doc """
    Index Handles the request GET /api/v1/customers, it loads data from
    the customer context/module.
  """
  @spec index(Plug.Conn.t, map) :: Plug.Conn.t
  def index(conn, _) do
    customers = Customer.all
    render(conn, "index.json", customers: customers)
  end

  @doc """
    Create handles the request POST /api/v1/customers, this is the simplist form
    of handling the request. The only required field is name.

    There's a lot more advanced ways of doing this depending our persistance stratigy,
    Ecto provides ChangeSets w/ validation - and is used when persisting to databases.

    The second create function will pattern match to any request that doesn't provide name,
    and will return bad_request.
  """
  @spec create(Plug.Conn.t, map) :: Plug.Conn.t
  def create(conn, %{"name" => _} = customer) do
    customer = struct(Customer, customer)
    with  %Customer{} = customer <- Customer.create(customer) do
      conn
      |> put_status(:created)
      |> render("show.json", customer: customer)
    end
  end
  def create(conn, _) do
    send_resp(conn, :bad_request, "")
  end

  @doc """
    Show handles the request GET /api/v1/customers/:id, this is pretty strait forward
    There's a few means control logic that would be available to us - With is commonly used
    by Elixir's success tuples {:ok, data} or {:error, reason}, so I wanted to show that.
    However, in this case a simple if statement would suffice.
  """
  @spec show(Plug.Conn.t, map) :: Plug.Conn.t
  def show(conn, %{"id" => id}) do
    with  %Customer{} = customer <- Customer.find(id) do
      render(conn, "show.json", customer: customer)
    else
      nil -> send_resp(conn, :not_found, "")
    end
  end

  @spec update(Plug.Conn.t, map) :: Plug.Conn.t
  def update(conn, %{"id" => id} =  customer_params) do
    customer_params = cast(customer_params)
    with %Customer{} = customer <- Customer.update(customer_params) do
      render(conn, "show.json", customer: customer)
    else
      nil -> send_resp(conn, :not_found, "")
    end
  end

  @doc """
  Delete handles the request DELETE /api/v1/customers/:id
  Not really much needs to be said about this.
  """
  @spec delete(Plug.Conn.t, map) :: Plug.Conn.t
  def delete(conn, %{"id" => id}) do
    with :ok <- Customer.delete(id) do
      send_resp(conn, :no_content, "")
    end
  end

  defp cast(%{"id" => id, "name" => name}), do: %{id: id, name: name}
  defp cast(%{"id" => id}), do: %{id: id}

end
