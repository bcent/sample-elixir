defmodule CustomerServiceWeb.Router do
  use CustomerServiceWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug CustomerService.Auth.Plug, []
  end

  scope "/api/v1", CustomerServiceWeb do
    pipe_through :api
    resources "/customers", CustomerController
  end
end
