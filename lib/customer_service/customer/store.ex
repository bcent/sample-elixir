defmodule CustomerService.Customer.Store do
  @moduledoc """
    Customer GenServer

    Stores Token
  """
  use GenServer

  alias CustomerService.Customer
  alias CustomerService.Customer.Repo
  require Logger

  @doc """
    StartLink - this method is called from the CustomerService.Application module,
    as part of the application boot. As a GenServer, it will run in it's own
    ErLang Process (light weight thread)
  """
  def start_link(default) do
    GenServer.start_link(__MODULE__, default, name: CustomerStore)
  end


  @doc """
    GenServer init implimentation
    After start link is called - init is called to setup the default state of the
    GenServer.  In the event that an application fails (due to an error) - it will be
    restarted immediately, and provide the crashed application's previous state.

    This also gets called in the event of code getting hot-swapped.

    Ultimately, we discard any provided state, and simply start with a blank repo.
  """
  @impl true
  def init(_) do
    {:ok, %Repo{}}
  end

  @doc """
    Fetch Handler - this will check the current state for the requested token,
    if it's not found it will grab it from the repo and load it into state.

    We will reply to the processes which spawned the message with the token.
  """
  @impl true
  def handle_call({:fetch, id}, _, state) do
    item = Map.get(state.data, id, nil)
    {:reply, item, state}
  end

  @doc """
    All Handler - this will take the data stored in state (as a map), and turn it into a list
    We reply to the process with this list.
  """
  @impl true
  def handle_call({:all}, _, state) do
    data = Enum.map(state.data, fn {_,v} -> v end)
    {:reply, data, state}
  end

  @doc """
    Create Handler - Create will take the data provided and coax it into the correct structure.
    It then inserts the data into the state.
  """
  @impl true
  def handle_call({:create, %{name: name}}, _, %{next_id: id} = state) do
    customer = %Customer{
      id: id,
      name: name,
      created_at: NaiveDateTime.utc_now,
      last_logged_in_at: NaiveDateTime.utc_now
    }

    customer
    |> write_data(state)
    |> increase_id
    |> wrap_reply({:data, customer.id})
  end

  @doc """
    Update handler - Update takes the data provided and uses it
    to update the state.
  """
  @impl true
  def handle_call({:update, customer}, _, state) do
    if Map.get(state.data, customer.id) |> is_nil do
      {:reply, nil, state}
    else
      state.data
      |> Map.get(customer.id)
      |> struct(customer)
      |> write_data(state)
      |> wrap_reply({:data, customer.id})
    end
  end

  @doc """
    Delete handler - Removes the data from the state.
  """
  @impl true
  def handle_call({:delete, id}, _, state) do
    data = Map.delete(state.data, id)
    state = Map.put(state, :data, data)
    {:reply, :ok, state}
  end

  @doc """
    Delete All handler - resets the state-data to an empty map.
    This will not change the next_id.
  """
  @impl true
  def handle_call({:delete_all}, _, state) do
    state = Map.put(state, :data, %{})
    {:reply, :ok, state}
  end

  defp increase_id(state) do
    Map.put(state, :next_id, state.next_id + 1)
  end
  defp write_data(%Customer{id: id} = customer, state) when is_nil(id) == false do
    data =
    state.data
    |> Map.put(id, customer)

    Map.put(state, :data, data)
  end

  defp wrap_reply(state, {:data, id}) do
    {:reply, Map.get(state.data, id), state}
  end
  defp wrap_reply(state, value) do
    {:reply, value, state}
  end
end
