defmodule CustomerService.Customer.Repo do
  @moduledoc """

  """
  alias CustomerService.Customer
  defstruct next_id: 1, data: %{}

  @spec save(Customer.t) :: :error | Customer.t
  def save(%Customer{id: nil}), do: :error
  def save(%Customer{} = customer) do
    customer
  end

  @spec delete(Customer.t) :: :ok | :error
  def delete(%Customer{id: nil}), do: :error
  def delete(%Customer{id: _id}) do
    :ok
  end

  @spec find(String.t) :: Customer.t | nil
  def find(_id) do
    nil
  end
end
