defmodule CustomerService.Customer do
  @moduledoc """
    Customer Context.

    In general Context are used to provide a top-level api to other parts of the app.
    In this case Customer Context manages dealing with Customers, we define the customer
    struct here, largely because it's a pretty simple context - that only really uses one
    struct. If you had something like Settings and Profile that also belonged to customers
    you'd want to define the stuctures as modules. (EG: CustomerService.Customer.Account,
    CustomerService.Customer.Setting, CustomerService.Customer.Profile)

    Most of the functions in this module are simple pass alongs, most often to GenServer
    back threads.
  """
  defstruct [:id, :name, :created_at, :last_logged_in_at]
  @type t :: %__MODULE__{
    id: pos_integer,
    name: String.t,
    created_at: NaiveDateTime.t,
    last_logged_in_at: NaiveDateTime.t
  }

  @doc """
    A simple proxy for the Customer.Store GenServer -
    This ultimately will invoke CustomerService.Customer.Store.handle_call({:all}, _, state)
  """
  @spec all() :: list(t)
  def all() do
    GenServer.call(CustomerStore, {:all})
  end

  @doc """
    A simple proxy for the Customer.Store GenServer -
    This ultimately will invoke CustomerService.Customer.Store.handle_call({:fetch, id}, _, state)

    This function does ensure that id is an integer as the top function will only match an integer id,
    where the bottom function will attempt to parse an integer out of anything else.
  """
  @spec find(integer | binary) :: t | nil
  def find(id) when is_integer(id)  do
    GenServer.call(CustomerStore, {:fetch, id})
  end
  def find(id) do
    {id, _} = Integer.parse(id)
    find(id)
  end

  @doc """
    A simple proxy for the Customer.Store GenServer -
    This ultimately will invoke CustomerService.Customer.Store.handle_call({:create, %Customer{}}, _, state)
  """
  @spec create(t) :: t
  def create(customer) do
    GenServer.call(CustomerStore, {:create, customer})
  end

  @doc """
    A simple proxy for the Customer.Store GenServer -
    This ultimately will invoke CustomerService.Customer.Store.handle_call({:update, %Customer{}}, _, state)
  """
  @spec update(t) :: t
  def update(%{id: id} = customer) when is_integer(id) do
    GenServer.call(CustomerStore, {:update, customer})
  end
  def update(%{id: id} = customer) do
    {id, _} = Integer.parse(id)
    customer
    |> Map.put(:id, id)
    |> update
  end

  @doc """
    A simple proxy for the Customer.Store GenServer -
    This ultimately will invoke CustomerService.Customer.Store.handle_call({:delete, id}, _, state)

    Like find/1, this ensures the ID is an integer
  """
  @spec delete(binary | integer) :: :ok | :error
  def delete(id) when is_integer(id)  do
    GenServer.call(CustomerStore, {:delete, id})
  end
  def delete(id) do
    {id, _} = Integer.parse(id)
    delete(id)
  end

  @doc """
      A simple proxy for the Customer.Store GenServer -
    This ultimately will invoke CustomerService.Customer.Store.handle_call({:delete_all}, _, state)
  """
  @spec delete_all() :: :ok
  def delete_all() do
    GenServer.call(CustomerStore, {:delete_all})
  end
end
