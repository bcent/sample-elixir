defmodule CustomerService.Auth do
  @moduledoc """
    Top Level Auth API, the only thing really exposed is the ability
    to fetch a token, from the app
  """

  alias Plug.Conn
  alias CustomerService.Auth.Token
  require Logger

  @doc """
    Fetchs a Token.

    This uses the Auth.Store GenServer.
  """
  @spec fetch_token(String.t) :: nil | Token.t
  def fetch_token(token) do
    GenServer.call(AuthStore, {:fetch, token})
  end

  @doc """
    Authenticates a connection has the requested permission.
    Otherwise, halt the request process and return a forbidden.

    The permissions are assigned by the CustomerService.Auth.Plug earlier in the
    life cycle.
  """
  @spec authenticate(Conn.t, String.t) :: Conn.t
  def authenticate(%{assigns: %{permissions: %Token{} = token}} = conn, permission) do
    if Enum.any?(token.permissions, fn x -> x == permission end) do
      conn
    else
      Logger.warn("No Permissions Found.")
      conn
      |> Conn.send_resp(:forbidden, "")
      |> Conn.halt
    end

  end
  def authenticate(conn, permission) do
    Logger.warn("must have #{permission} privilege")
    conn
    |> Conn.send_resp(:forbidden, "")
    |> Conn.halt
  end
end
