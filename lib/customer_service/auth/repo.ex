defmodule CustomerService.Auth.Repo do
  @moduledoc """
    Auth Repo - pretty primative since this is read only from a file.
  """

  @doc """
    Looks up a token, and returns it's permissions
    - Add in a 1 second delay to similuate a slow service.
  """
  alias CustomerService.Auth.Token
  def find(id) do
    Process.sleep(1000)

    File.cwd!
    |> Path.join("priv/data/examplePrivlages.yml")
    |> YamlElixir.read_from_file!
    |> Map.get(id)
    |> wrap(id)
  end

  @doc """
    Wrap takes the list of permissions and generates a token.
    If it's not provided, it will return nil.
  """
  defp wrap(nil, _), do: nil
  defp wrap(permissions, id) do
    %Token{
      id: id,
      permissions: permissions,
      read_at: DateTime.utc_now |> DateTime.to_unix

    }
  end
end
