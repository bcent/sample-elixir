defmodule CustomerService.Auth.Plug do
  @moduledoc """
    Auth.Plug - A Cowboy / Phoenix plugin for Authenticating.
  """
  import Plug.Conn
  require Logger
  alias Plug.Conn
  alias CustomerService.Auth

  # We're using the plug behavior, which means we'll need to specify
  # init/1 and call/2
  @behaviour Plug

  @doc """
    Init formats the opts passed in as a second param to call.
    As we don't need any options, we can just accept anything, and
    pass along a nil value.
  """
  @spec init(any) :: nil
  @impl true
  def init(_), do: nil

  @doc """
    Call is as part of the Router's Pipeline (:api).
  """
  @spec call(Conn.t, nil) :: Conn.t
  @impl true
  def call(%{} = conn, _) do
    conn
    |> fetch_token
    |> handle_token(conn)
  end

  @doc """
    Fetch token takes a connection, pulls the auth token from it,
    and attempts to fetch a token with that id.
  """
  @spec fetch_token(Conn.t) :: nil | CustomerService.Auth.Token.t
  def fetch_token(%Conn{req_headers: headers} = conn) do
    headers
    |> Enum.into(%{})
    |> Map.get("auth_token")
    |> Auth.fetch_token
  end

  @doc """
    Handle token takes the token provided, and acts on the connection,
    either assigning the token to it, or sending an unauthorized response.
  """
  @spec handle_token(CustomerService.Auth.Token.t | nil, Conn.t) :: Conn.t
  def handle_token(nil, conn) do
    token = conn.req_headers
    |> Enum.into(%{})
    |> Map.get("auth_token")

    Logger.warn("Token #{token} not valid")

    conn
    |> send_resp(:unauthorized, "")
    |> halt
  end
  def handle_token(token, conn), do: assign(conn, :permissions, token)

end
