defmodule CustomerService.Auth.Store do
  @moduledoc """
    Auth GenServer

    Stores Token
  """
  use GenServer

  # IF we  wanted to use State as a cache for external data - we could setup
  # some module variables to control it's behavior like such, we'd also need to build
  # the functionality to clean up the state at an interval.
  #
  # - @state_ttl 86400
  #
  # Of course that means we'd also need to impliment eviction.


  alias CustomerService.Auth.{Token, Repo}

  @doc """
    StartLink - starts up a genserver (it's own process), and uses
    a name - meaning it's a single process for the entire VM.
  """
  def start_link(default) do
    GenServer.start_link(__MODULE__, default, name: AuthStore)
  end

  @doc """
    GenServer init implimentation - used to set the starting state
    If we were truely using YAML for storing tokens, we could read the file
    in here to load it on startup. However, since we need to similate a slow
    service, a blank map works better.
  """
  @impl true
  def init(_) do
    {:ok, %{}}
  end

  @doc """
    Fetch Handler - this will check the current state for the requested token,
    if it's not found it will grab it from the repo and load it into state.

    We will reply to the processes which spawned the message with the token.
  """
  @impl true
  def handle_call({:fetch, token}, _, state) do
    state
    |> Map.get(token)
    |> reply(state, token)
  end

  @doc """
    Formats a Reply
    If needed it will fetch and set a token
  """
  def reply(nil, state, token) do
    token
    |> fetch
    |> set_token(state)
  end
  def reply(token_struct, state, _), do: {:reply, token_struct, state}

  def set_token(nil, state), do: {:reply, nil, state}
  def set_token(%Token{} = token, state) do
    state = Map.put(state, token.id, token)
    {:reply, token, state}
  end

  defp fetch(id) do
    Repo.find(id)
  end


end
