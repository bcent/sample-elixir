defmodule CustomerService.Auth.Token do
  @moduledoc """
    Auth Token
  """
  defstruct [:id, :read_at, permissions: []]
  @type t :: %__MODULE__{
    id: String.t,
    permissions: list(String.t),
    read_at: pos_integer
  }
end
