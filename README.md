# CustomerService

## Running (with Docker)

Running Tests

`docker run -it -v ${PWD}:/opt/app -w /opt/app -e MIX_ENV=test elixir:1.7-slim mix do local.hex --force, deps.get, test`

Running Server (Port 4000)

`docker run -it -v ${PWD}:/opt/app  -p 4000:4000 -w /opt/app elixir:1.7-slim mix do local.hex --force, deps.get, phx.server`

Generate Docs (html in the docs folder)

`docker run -it -v ${PWD}:/opt/app  -p 4000:4000 -w /opt/app elixir:1.7-slim mix do local.hex --force, docs`

## Request / Flow Lifecycle

1. Router - Request start at the `lib/customer_service_web/router.ex`
2. Router Pipeline - A Series of Plug based Middleware. Which will include `lib/customer_service/auth/plug.ex`
3. Controller Middleware - The middleware specified by the controller, which includes `lib/customer_service/auth.ex` (specifically the authenticate/2 method)
4. Controller - `lib/customer_service_web/controllers/customer_controller.ex`
5. Application Logic - (Customer CRUD)  `lib/customer_service/customer.ex`
6. JSON Trasnformation - `lib/customer_service_web/views/customer_view.ex`

## Tasks

**Fix a bug where a new customer is being created even when the requester has insufficient privileges.**

This bug seemed to be caused by ReactiveX's subscription not handling errors as expected, which resulted in executing the create process regardless. 

Elixir's Plug (middleware), makes it possible to intercept the requests before it gets into the action code. We're also able to have all the authentication logic bundled closer together. Which helps alleviate some of the "where is this error coming from..." logic. 

**Fix a bug where new customers are being created with duplicate IDs.**

This bug was caused by issues with concurrency and memory management, the quick solution being to use AtomicLong over a standard Long. 

More or less the problem doesn’t exist in Elixir persistent state is kept in GenServer's which are their own dedicated Erlang Process, and communicate via messages. This however means that you will run into performance problems before you run into consistency problems. And that only happens when you use blocking IO in the GenServer and don't account for it by using async code inside the GenServer (the invoking code treats all GenServer calls the same)

**Implement caching to improve performance when getting privileges for an auth token.**

While I never got around to implementing this in the Koltin project, my thoughts were to use an in-memory store similar to the one provided for Customers.

In Elixir I mirrored the CustomerStore, with the AuthStore... mainly a GenServer. Without understanding the nature of the 'SlowPrivilegesRepository', I opted for not evicting tokens. Having an idea of the general quantity of tokens, along with if they can be revoked would influence decisions here. Much like the JVM Languages, Elixir has libraries that can provide these functions as well - both Erlang Table Storage (in Memory Cache), and external cache (Redis, MemCached, Etc ...) would be workable solutions. 

**Bonus: Implement an alternate CustomerRepository that persists customers data.**

I didn't manage to get to this as well. 

I actually wanted to avoid persistence in this project, as the blocking IO would mean implementing async functionality inside of the CustomerRepository GenServer. After ReactiveX left me feeling like a complete idiot - I thought going with the simple implementation might be best. If you want to see what it would look like, or want to talk about how it works ... I'm happy to do so.  

### So what the hell is a |> 

Since it's used all over the place, this is the best place for me to explain the pipe operator ( |> ). 
It takes the return value and uses it as the first parameter in the next function. 
EG value |> function(test) - could also be written as function(value, test). This can be chained as many 
times as you like. EG: 

```elixir
    data = 
    value
    |> add(test)
    |> check(:all)
    |> filter(:error)
```

could be written as ... 

```elixir
    temp = add(value, test)
    temp2 = check(temp, :all)
    data = filter(temp2, :error)
```

... or 

```elixir
    data = filter(check(add(value, test), :all), :error)
```


